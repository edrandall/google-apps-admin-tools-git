#
# Display groups.list using "jinja2" template
# see https://developers.google.com/apis-explorer/#p/admin/directory_v1/directory.groups.list
# (c) 2014 Ed Randall
#

import cgi
import jinja2
import logging
import os
import urllib
import webapp2
import datetime
import urllib
import json

from google.appengine.api import urlfetch
from templates import *

class UsersList(webapp2.RequestHandler):
	def post(self):
	
		template_values = {
			'now': datetime.datetime.now(),
			'status': 0,
			'message': 'None',
			'list': []
		}
		
		domain = self.request.get('domain') 
		authtoken = self.request.get('authtoken')
		logging.info("POST request: domain="+str(domain) +" token="+str(authtoken))
		
		if domain == None:
			template_values['status_code'] = 405
			template_values['status_message'] = 'No domain'
		
		elif authtoken == None:
			template_values['status_code'] = 401
			template_values['status_message'] = 'No authentication token'
			
		else:
			result = self.queryAllUsers(domain, authtoken)
			template_values['status_code'] = result['status_code']
			template_values['status_message'] = result['status_message']
			template_values['list'] = result['users']
			
		template_values['now'] = datetime.datetime.now()

		template = templates.get_template('users.html')
		self.response.out.write(template.render(template_values))

		
	def queryAllUsers(self, domain, token):
		result = {
			'status_code': 0,
			'status_message': 'None',
			'users': []
		}
		
		# A query to get the list of all users
		# Requires oauth2 scope https://www.googleapis.com/auth/admin.directory.user.readonly
		params = urllib.urlencode({
			'domain': domain
		})
		queryurl = self.app.config.get('gapi_users') +"?" +params
		
		response = urlfetch.fetch(
			url = queryurl,
			method = urlfetch.GET,
			follow_redirects = False,
			headers = {
				'Authorization': token
			}
		)
		logging.info("Called "+str(queryurl)+" response="+str(response.status_code))
		result['status_code'] = response.status_code
		if (response.status_code != 200):
			data = json.loads( response.content )
			result['status_message'] = data['error']['message']
			result['users'] = []
			
		else:
			data = json.loads( response.content )
			result['status_message'] = 'OK'
			result['users'] = data['users']
				
		return result
