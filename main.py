#!/usr/bin/env python
# -*- coding: utf-8 -*-

import webapp2
from appstools import *

import ConfigParser

config = ConfigParser.SafeConfigParser()
config.read('config/config.ini')

# Map URLs to application functions
url_list = [
	(r'/admin/groups', GroupsList),
	(r'/admin/users', UsersList)
	]

# Main entry point - see app.yaml
app = webapp2.WSGIApplication(
	url_list,
	debug = True,
	config = dict( config.items('config') ))
